struct Ejer {
              int id;
              char *nombre;
              char Estado;
};

void sc_introducirID (struct Ejer, int);
void sc_introducirNombre(struct Ejer, char*);
void sc_Estado(struct Ejer, char );

int sc_getID(struct Ejer);
char *sc_getNombre(struct Ejer);

char sc_Estado(struct Ejer);
void sc_crearEjer(struct Ejer, int, char*);
void sc_ejecutarEje(struct Ejer);